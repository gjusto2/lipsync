﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using Random = UnityEngine.Random;
using TouchPhase = UnityEngine.InputSystem.TouchPhase;

// Used for the Hat selection logic
public class PlayerConfigurator : MonoBehaviour
{
    [SerializeField]
    private Transform m_HatAnchor;
    // [SerializeField] private AssetReferenceGameObject _hatAssetReference;
    [SerializeField] private GameObject _hatInstance;

    void Start()
    {           
        // SetHat(string.Format("Hat{0:00}", GameManager.s_ActiveHat));
        LoadInRandomHat();
    }

    // public void SetHat(string hatKey)
    // {
    //     if (!_hatAssetReference.RuntimeKeyIsValid())
    //     {
    //         return;
    //     }
    //     _hatLoadOpHandler = Addressables.LoadAssetAsync<GameObject>(_hatAssetReference);
    //     _hatLoadOpHandler.Completed += OnHatLoadComplete;
    // }

    public void LoadInRandomHat()
    {
        int randoIndex = Random.Range(0, 6);
        string hatAddress = string.Format("Hat{0:00}", randoIndex);

    }

    // private void OnHatLoaded(AsyncOperation asyncOperation)
    // {
    //     Instantiate(m_HatLoadingRequest.asset as GameObject, m_HatAnchor, false);
    // }
    private void Update()
    {
        if (Touchscreen.current.primaryTouch.press.wasReleasedThisFrame )
        {
            Destroy(_hatInstance);
            LoadInRandomHat();
        }
    }

    private void OnDisable()
    {

    }
}
