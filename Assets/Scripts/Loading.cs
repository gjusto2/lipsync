﻿using System.Collections;
using UnityEngine;

using UnityEngine.UI;

public class Loading : MonoBehaviour
{
    private AsyncOperation m_SceneOperation;

    [SerializeField]
    private Slider m_LoadingSlider;

    [SerializeField]
    private GameObject m_PlayButton, m_LoadingText;

    private void Awake()
    {
        StartCoroutine(loadNextLevel("Level_0" + GameManager.s_CurrentLevel));
    }

    private IEnumerator loadNextLevel(string level)
    {


            yield return null;

        Debug.Log($"Loaded Level {level}");
    }

    // Function to handle which level is loaded next
    // public void GoToNextLevel()
    // {
    //     _sceneLoadOpHandle.allowSceneActivation = true;
    // }
}
